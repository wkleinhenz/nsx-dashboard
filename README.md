#NSX Dashboard

A companion to the VMWare NSX Cribl app located here(https://gitlab.com/wkleinhenz/cc-vmware-nsx-syslog-cribl)
This provides a set of usable dashboards based on the VMware NSX App for splunk but with the following differences
* uses CIM compliant fields
* uses refined SPL, less usage of Regex when possible, using index and source type to reduce search impact
* Created in Dashboard studio so its usable with Splunk 9.X
